import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.*;
import javax.swing.*;
import java.util.*;
import java.io.*;


public class MemoryApp extends JFrame {

	static String Pics[] = {"1.JPG", "2.jpg","3.jpg", "4.jpg","5.jpg", "6.jpg"};
	static JButton buttons[];
	ImageIcon closedIcon;
	int numButtons;
	ImageIcon icons[];
	int firstClick = 555;
	int secondClick = 555;
	ArrayList<Integer> saveMatch = new ArrayList<Integer>();
	int openImages;
	static int WIDTH = 50;
	static int HEIGHT = 50;
    
    static JPanel jImg;
    static JPanel btnPanel;
    static JFrame jf;

	public MemoryApp() {
        
        btnPanel = new JPanel(new BorderLayout());
        jf = new JFrame();
        jf.setSize(500,450);
		jf.setVisible(true);
		jf.setLocation(450, 220);
		jf.setTitle("Memory Game");
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		String[] comboTypes = { "4", "5", "6" };
		JComboBox comboTypesList = new JComboBox(comboTypes);
		comboTypesList.setSelectedIndex(2);
		comboTypesList.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				JComboBox jcmbType = (JComboBox) e.getSource();
				String cmbType = (String) jcmbType.getSelectedItem();
				jf.getContentPane().add(new Img(Integer.parseInt(cmbType)));
			}
		});
        
		btnPanel.add(comboTypesList,BorderLayout.PAGE_END);
		jf.getContentPane().add(btnPanel);
		jf.pack();
		jf.setVisible(true);
		jf.validate();		
	}

	private class Img extends JPanel
	{
		public Img(int num)
		{
		int selected = num;
        String temp_pic[] = new String[selected];

        System.arraycopy(Pics,0,temp_pic,0,selected);
        Pics = temp_pic;


        setLayout(new GridLayout(3,4));

		closedIcon = new ImageIcon("close.jpg");
		Image img = closedIcon.getImage() ;  
   	    Image newimg = img.getScaledInstance( WIDTH, HEIGHT,  java.awt.Image.SCALE_SMOOTH ) ;
   	    closedIcon  = new ImageIcon( newimg );
		numButtons = Pics.length * 2;
		buttons = new JButton[numButtons];
		icons = new ImageIcon[numButtons];
		for (int i = 0, j = 0; i < Pics.length; i++) 
		{
			icons[j] = new ImageIcon(Pics[i]);
			img = icons[j].getImage() ;  
   			newimg = img.getScaledInstance( WIDTH, HEIGHT,  java.awt.Image.SCALE_SMOOTH ) ;  
   			icons[j] = new ImageIcon( newimg );
			buttons[j] = new JButton("");
			buttons[j].setBounds(new Rectangle(50,50));
			buttons[j].addActionListener(new MemoryApp.ImageButtonListener());
			buttons[j].setIcon(closedIcon);
			add(buttons[j++],BorderLayout.CENTER);

			icons[j] = icons[j - 1];
			buttons[j] = new JButton("");
			buttons[j].setBounds(new Rectangle(50,50));
			buttons[j].addActionListener(new MemoryApp.ImageButtonListener());
			buttons[j].setIcon(closedIcon);
			add(buttons[j++],BorderLayout.CENTER);
		}

        
        
		// randomize icons
		Random generator = new Random();
		for(int i=0 ; i<numButtons; i++)
		{
			int j = generator.nextInt(numButtons);
			ImageIcon temp = icons[i];
			icons[i] = icons[j];
			icons[j] = temp;
		}    
		// Pack and display the window.
		jf.getContentPane().add(jImg);
		jf.pack();
		jf.setVisible(true);
		jf.validate();	
		}
		
	}

	

	private class ImageButtonListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {

            if( firstClick != 555 && secondClick != 555 )
            {
				if( buttons[firstClick].getIcon() != buttons[secondClick].getIcon() )
            	{
            		buttons[firstClick].setIcon(closedIcon);
            		buttons[secondClick].setIcon(closedIcon);
            	}
            	firstClick = secondClick = 555;
            }

			for (int i = 0; i < numButtons; i++) 
			{
				if (e.getSource() == buttons[i] ) 
				{
					buttons[i].setIcon(icons[i]);

                    if( firstClick == 555 )
                    	firstClick = i;
                    else
                        secondClick = i;
				}
			}
		}
	}
    
    
	public static void main(String[] args) {
        
        MemoryApp mem = new MemoryApp();
	}
}