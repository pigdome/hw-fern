import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import java.awt.*;
import javax.swing.*;
import java.util.*;
import java.io.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class MoveCoin extends JPanel {

    
    private Point p1 = new Point(100, 100);
    private Point p2 = new Point(540, 380);
    private Point circleActive = p1;
    private String colorList[] = {"black","red","green","blue"};
    private String colorC1 = "red";
    private String colorC2 = "green";

    private boolean drawing;

    public MoveCoin() {
        this.setPreferredSize(new Dimension(640, 480));
        
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        //---- circle 1
        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(getColor(colorC1));
        g2d.setRenderingHint(
            RenderingHints.KEY_ANTIALIASING,
            RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setStroke(new BasicStroke(1,
            BasicStroke.CAP_ROUND, BasicStroke.JOIN_BEVEL));
        g.fillOval(p1.x,p1.y,20,20);
        
        //---- circle 2
        g2d = (Graphics2D) g;
        g2d.setColor(getColor(colorC2));
        g2d.setRenderingHint(
            RenderingHints.KEY_ANTIALIASING,
            RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setStroke(new BasicStroke(1,
            BasicStroke.CAP_ROUND, BasicStroke.JOIN_BEVEL));
        g.fillOval(p2.x,p2.y,40,40);
    }

    
    private class ControlPanel extends JPanel {

        private static final int DELTA = 5;

        public ControlPanel() 
        {
            this.add(new MoveButton("\u2190", KeyEvent.VK_LEFT, -DELTA, 0));
            this.add(new MoveButton("\u2191", KeyEvent.VK_UP, 0, -DELTA));
            this.add(new MoveButton("\u2192", KeyEvent.VK_RIGHT, DELTA, 0));
            this.add(new MoveButton("\u2193", KeyEvent.VK_DOWN, 0, DELTA));

            
            JComboBox btncolorList_s = new JComboBox(colorList);
            btncolorList_s.addActionListener(new ActionListener() 
            {

                public void actionPerformed(ActionEvent e) {
                    JComboBox jcmbType = (JComboBox) e.getSource();
                    String cmbType = (String) jcmbType.getSelectedItem();
                       colorC1 = cmbType;
                       repaint();
                }
            });

            JComboBox btncolorList_b = new JComboBox(colorList);
            btncolorList_b.addActionListener(new ActionListener() 
            {

                public void actionPerformed(ActionEvent e) {
                    JComboBox jcmbType = (JComboBox) e.getSource();
                    String cmbType = (String) jcmbType.getSelectedItem();
                       colorC2 = cmbType;
                       repaint();
                }
            });

            this.add(btncolorList_s);
            this.add(btncolorList_b);

            JRadioButton firstButton = new JRadioButton("Small Coin");
            firstButton.setActionCommand("Small Coin");
            firstButton.setSelected(true);

            JRadioButton secondButton = new JRadioButton("Big Coin");
            secondButton.setActionCommand("Big Coin");

            // Group the radio buttons.
            ButtonGroup group = new ButtonGroup();
            group.add(firstButton);
            group.add(secondButton);

            // Register a listener for the radio buttons.
            RadioListener myListener = new RadioListener();
            firstButton.addActionListener(myListener);
            secondButton.addActionListener(myListener);
            this.add(firstButton);
            this.add(secondButton);

            
        }

        private class RadioListener implements ActionListener
        {  
            public void actionPerformed(ActionEvent e) 
            {
                if (e.getActionCommand() == "Small Coin") 
                    circleActive = p1;
                else 
                {
                    circleActive = p2;
                }
        
            }
        }
        private class MoveButton extends JButton {

            KeyStroke k;
            int dx, dy;

            public MoveButton(String name, int code,
                    final int dx, final int dy) {
                super(name);
                this.k = KeyStroke.getKeyStroke(code, 0);
                this.dx = dx;
                this.dy = dy;
                this.setAction(new AbstractAction(this.getText()) {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        Point temp1 = new Point(p1.x,p1.y);
                        Point temp2 = new Point(p2.x,p2.y);

                        MoveCoin.this.circleActive.translate(dx, dy);
                        //MoveCoin.this.repaint();
                        double distance = Math.pow(p1.x-p2.x,2) + Math.pow(p1.y-p2.y,2);
                        distance = Math.sqrt(distance);
                        if( distance > 30 )
                        {    
                            if( circleActive.x > 0 && circleActive.x < 640 && circleActive.y > 0 && circleActive.y < 480 )
                                MoveCoin.this.repaint();
                            else
                            {
                                p1 = temp1;
                                p2 = temp2;
                                MoveCoin.this.repaint();
                            }
                        }       
                        else
                        {
                            p1 = temp1;
                            p2 = temp2;
                            MoveCoin.this.repaint();
                        }
                    }
                });
                ControlPanel.this.getInputMap(WHEN_IN_FOCUSED_WINDOW)
                    .put(k, k.toString());
                ControlPanel.this.getActionMap()
                    .put(k.toString(), new AbstractAction() {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        MoveButton.this.doClick();
                    }
                });
            }
        }
    }


    private void display() {
        JFrame f = new JFrame("MoveCoin");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.add(this);
        f.add(new ControlPanel(), BorderLayout.SOUTH);
        f.pack();
        f.setLocationRelativeTo(null);
        f.setVisible(true);
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
                new MoveCoin().display();
            }
        });
    }

    public Color getColor(String color)
    {
        switch(color)
        {
            case "red"   : return Color.red;
            case "green" : return Color.green;
            case "blue"  : return Color.blue;
            case "black" : return Color.black;
        }
        return Color.black;
    }
}