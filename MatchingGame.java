import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.*;
import javax.swing.*;
import java.util.*;
import java.io.*;

public class MatchingGame {
    
    static String Pics[] = {"1.JPG", "2.jpg","3.jpg", "4.jpg","5.jpg", "6.jpg"};
    static JButton buttons[];
    ImageIcon closedIcon;
    int numButtons;
    ImageIcon icons[];
    int firstClick = 555;
    int secondClick = 555;
    static int WIDTH = 100;
    static int HEIGHT = 100;
    int lastSelected = 4;
    boolean notpain = true;
    JFrame frame;
    int index = 0;

    public void addComponentsToPane(Container pane) 
    {
        
        pane.setComponentOrientation(java.awt.ComponentOrientation.RIGHT_TO_LEFT);
        
        String[] comboTypes = { "4", "5", "6" };
        JComboBox comboTypesList = new JComboBox(comboTypes);
        comboTypesList.setSelectedIndex(index);
        comboTypesList.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                JComboBox jcmbType = (JComboBox) e.getSource();
                String cmbType = (String) jcmbType.getSelectedItem();
                lastSelected = Integer.parseInt(cmbType);

                switch( lastSelected )
                {
                    case 4 : index = 0;break;
                    case 5 : index = 1;break;
                    case 6 : index = 2;break;
                }
                notpain = false;
                frame.dispose();
                createAndShowGUI();
            }
        });

        pane.add(comboTypesList, BorderLayout.PAGE_START);
        
        JButton button = new JButton("New Game");
        button.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) 
            {
                  notpain = false;
                  //frame.dispose();
                  createAndShowGUI();
            }
        });
        
        pane.add(button,BorderLayout.PAGE_END);
        //Make the center component big, since that's the
        //typical usage of BorderLayout.
        if( notpain )
        {
            JPanel jpn = new JPanel();
            jpn.setPreferredSize(new Dimension(470, 350));
            pane.add(jpn, BorderLayout.CENTER);
        }
        else
        {
            int selected = lastSelected;
            String temp_pic[] = new String[selected];
            System.arraycopy(Pics,0,temp_pic,0,selected);
            

            JPanel jImg = new JPanel();
            jImg.setLayout(new GridLayout(3,4));
            jImg.setPreferredSize(new Dimension(470,350));
            closedIcon = new ImageIcon("close.jpg");
            Image img = closedIcon.getImage() ;  
            Image newimg = img.getScaledInstance( WIDTH, HEIGHT,  java.awt.Image.SCALE_SMOOTH ) ;
            closedIcon  = new ImageIcon( newimg );
            numButtons = temp_pic.length * 2;
            buttons = new JButton[numButtons];
            icons = new ImageIcon[numButtons];
            for (int i = 0, j = 0; i < temp_pic.length; i++) 
            {
                icons[j] = new ImageIcon(temp_pic[i]);
                img = icons[j].getImage() ;  
                newimg = img.getScaledInstance( WIDTH, HEIGHT,  java.awt.Image.SCALE_SMOOTH ) ;  
                icons[j] = new ImageIcon( newimg );
                buttons[j] = new JButton("");
                buttons[j].setBounds(new Rectangle(50,50));
                buttons[j].addActionListener(new MatchingGame.ImageButtonListener());
                buttons[j].setIcon(closedIcon);
                jImg.add(buttons[j++],BorderLayout.CENTER);

                icons[j] = icons[j - 1];
                buttons[j] = new JButton("");
                buttons[j].setBounds(new Rectangle(50,50));
                buttons[j].addActionListener(new MatchingGame.ImageButtonListener());
                buttons[j].setIcon(closedIcon);
                jImg.add(buttons[j++],BorderLayout.CENTER);
            }

            
            
            // randomize icons
            Random generator = new Random();
            for(int i=0 ; i<numButtons; i++)
            {
                int j = generator.nextInt(numButtons);
                ImageIcon temp = icons[i];
                icons[i] = icons[j];
                icons[j] = temp;
            }
            pane.add(jImg,BorderLayout.CENTER);
        }        
    }
    
    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event dispatch thread.
     */
    private void createAndShowGUI() {
        
        //Create and set up the window.
        
        JFrame temp = frame;    
        frame = new JFrame("MatchingGame");
        if( ! notpain )
            temp.dispose();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //Set up the content pane.
        addComponentsToPane(frame.getContentPane());
        //Use the content pane's default BorderLayout. No need for
        //setLayout(new BorderLayout());
        //Display the window.
        frame.pack();
        frame.setVisible(true);
    }
    
    public static void main(String[] args) {
        
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
               new MatchingGame().createAndShowGUI();
            }
        });
    }

   
    
    private class ImageButtonListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {

            if( firstClick != 555 && secondClick != 555 )
            {
                if( buttons[firstClick].getIcon() != buttons[secondClick].getIcon() )
                {
                    buttons[firstClick].setIcon(closedIcon);
                    buttons[secondClick].setIcon(closedIcon);
                }
                firstClick = secondClick = 555;
            }

            for (int i = 0; i < numButtons; i++) 
            {
                if (e.getSource() == buttons[i] ) 
                {
                    buttons[i].setIcon(icons[i]);

                    if( firstClick == 555 )
                        firstClick = i;
                    else
                        secondClick = i;
                }
            }
        }
    }
}
