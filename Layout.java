import java.util.*;
import java.awt.*;
import javax.swing.*;

public class Layout{
	
	private int matches;
	private int attempts;
	
	public void jpanel(){
		JFrame frame = new JFrame();
		JPanel gridPanel = new JPanel(new GridLayout(5, 6));
		JPanel northPanel = new JPanel();
		JButton[][] button = new JButton[6][5];
		
		JButton attempts = new JButton("Attempts: ");
		JButton playAgain = new JButton("Play Again");
		JButton matches = new JButton("Matches: ");
		northPanel.add(attempts);
		northPanel.add(matches);
		northPanel.add(playAgain);
		frame.setSize(900, 700);
		frame.add(northPanel, BorderLayout.NORTH);
		northPanel.setSize(900, 100);
		frame.add(gridPanel);
		gridPanel.setSize(900, 600);
		gridPanel.setBackground(Color.blue);
		for(int i=0; i<button.length; i++){
			for(int j=0; j<button[i].length; j++){
				int n = i*button[i].length + j+1;
				button[i][j] = new JButton(String.valueOf(n));
				gridPanel.add(button[i][j]);
			}
		}
		java.net.URL image = Memory.class.getResource("phillies.jpg");
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
