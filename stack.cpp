#include <iostream.h>
#include <iostream>
#include <conio.h>
#include <string>
#include <stdlib.h>
using namespace std;


static int t=0;
static int k=0;
class stack_char
{
	
	char s[20];    
	public:
	void push(char y);
	char pop();
	char top();
	bool empty();
	void show();
	
};

void stack_char::push(char y)
{
	if(t<20)
	{
		t=t+1;
		s[t]=y;
	}
	else
		cout<<endl<<"stack overflows...cpush"<<endl;
}

char stack_char::pop()
{
	char item;
	if(t>=0)
	{
		t=t-1;
		item=s[t+1];
		return item;
	}
	else
		cout<<endl<<"stack underflows...cpop"<<endl;
	return 0;
}

char stack_char::top()
{
	if(t>=0)
		return s[t];
	else
		cout<<endl<<"stack underflows...ctop"<<endl;
	return 0;
}

bool stack_char::empty()
{
	if(t<=0)
		return true;
	else
		return false;
}

//--------------------------------------------------------------------------

class stack_double
{
	
	double s[20];    
	public:
	void push(double y);
	double pop();
	double top();
	bool empty();
	void show();
	
};

void stack_double::push(double y)
{
	if(k<20)
	{
		k=k+1;
		s[k]=y;
	}
	else
		cout<<endl<<"stack overflows...dpush"<<endl;
}

double stack_double::pop()
{
	double item;
	if(k>=0)
	{
		k=k-1;
		item=s[k+1];
		return item;
	}
	else
		cout<<endl<<"stack underflows...dpop"<<endl;
	return 0;
}

double stack_double::top()
{
	if(k>=0)
		return s[k];
	else
		cout<<endl<<"stack underflows...dtop"<<endl;
	return 0;
}

bool stack_double::empty()
{
	if(k<=0)
		return true;
	else
		return false;
}

//--------------------------------------------------------------------------
class SimpleCalculator
{
	public:
	//SimpleCalculator();
	//~SimpleCalculator();
	string infixToPostfix(string infix);
    double posfixToDouble(string posfix);
    char addOperatorToStack(char c,string posfix);
    bool isOperator(char c);
    stack_char s_operator;
    stack_double number;
};

/*SimpleCalculator()
{
  
}
~SimpleCalculator()
{

}*/
string SimpleCalculator::infixToPostfix(string infix)
{
	string posfix = "";
    for(int i = 0; i < infix.length(); i++)
    {
    	char c = infix[i];
    	char op;
    	switch(c)
    	{
    		case '+' : op = addOperatorToStack(c,posfix);if( op != '#' ) posfix += op;break;
    		case '-' : op = addOperatorToStack(c,posfix);if( op != '#' ) posfix += op;break;
    		case '*' : op = addOperatorToStack(c,posfix);if( op != '#' ) posfix += op;break;
    		case '/' : op = addOperatorToStack(c,posfix);if( op != '#' ) posfix += op;break;
    		case '(' : op = addOperatorToStack(c,posfix);if( op != '#' ) posfix += op;break;
    		case ')' : op = addOperatorToStack(c,posfix);if( op != '#' ) posfix += op;break;
    		default:
    		{
    		   if( i > 0 && infix[i-1] == ')' )
    		      posfix += addOperatorToStack('*',posfix);
    		   posfix += c; // char is number
    		}
    	}
	}
	while(! s_operator.empty())
	{
		if( s_operator.top() == '+' || s_operator.top() == '-' || s_operator.top() == '*' || s_operator.top() == '/' )
		  posfix += s_operator.pop();
		else
		  s_operator.pop();
	}
	return posfix;
}

char SimpleCalculator::addOperatorToStack(char c,string posfix)
{
    char op = '#';
    if( c == '+' || c == '-' || c == '*' || c == '/' )
    {
    	if( s_operator.top() == ')' )
    	{
    		s_operator.pop();
    		op = s_operator.pop();
    		s_operator.pop();
    		s_operator.push(c);
    	}
    	else if( s_operator.top() == '(' )
    	{
    		s_operator.push(c);
    	}
    	else if( s_operator.top() == '+' || s_operator.top() == '-' || s_operator.top() == '*' || s_operator.top() == '/' )
    	{
    		op = s_operator.pop();
    		s_operator.push(c);
    	}
    	else
    		s_operator.push(c);
    	
    }
    else if( c == ')' && s_operator.top() == ')' )
    {
    	s_operator.pop();
		op = s_operator.pop();
		s_operator.pop();
		s_operator.push(c);
    }
    else
        s_operator.push(c);

    return op;
}

double SimpleCalculator::posfixToDouble(string posfix)
{
    for(int i = 0; i < posfix.length(); i++)
    {
    	char c = posfix[i];
    	if( isOperator(c) )
    	{
            double a = number.pop();
            double b = number.pop();
            switch(c)
            {
            	case '+' : number.push(a+b);break;
            	case '-' : number.push(b-a);break;
            	case '*' : number.push(a*b);break;
            	case '/' : number.push(b/a);break;
            }
    	}
    	else
    	{
    		if( c != '#' )
    		{
    			int i = c-'0';
                number.push(i);
    		}
    		
    	}
    }
    return number.pop();
}

bool SimpleCalculator::isOperator(char c)
{
	if(  c == '+' || c == '-' || c == '*' || c == '/' || c == '(' || c == ')' )
		return true;
	else
		return false;
}


//--------------------------------------------------------------------------

int main()
{
	SimpleCalculator sc;
	string infix;
	string posfix;

	while( true )
	{
		cout << " Type your infix expression or # to quit :";
		cin >> infix;
		if( infix.compare("#") == 0 )
			break;
		posfix = sc.infixToPostfix(infix);
		cout << " Convert to Postfix : " << posfix << endl;
		double result = sc.posfixToDouble(posfix);
		cout << " The answer is " << result;
	}

	return 0;

}
